package com.example._05_kps_ivchanarun_springboot_hw2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ListController {
    private final List<Subjectlist> subjectlists = new ArrayList<>();

    ListController() {
        subjectlists.add(new Subjectlist(1, "java", "book"));
        subjectlists.add(new Subjectlist(2, "web", "book"));
        subjectlists.add(new Subjectlist(3, "korea", "book"));

    }

    @GetMapping()
    public ModelAndView getHomePage() {
        System.out.println(subjectlists);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        mv.addObject("Subject", subjectlists);
        return mv;
    }

    @GetMapping("Card")
    public ModelAndView getCardPage() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("Card");
        return mv;
    }

    @GetMapping("/Register")
    public ModelAndView getRegister() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("Register");
        return mv;
    }

    @PostMapping("/insert")
    public ModelAndView insertNewSubject() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/");
        return mv;
    }
}



